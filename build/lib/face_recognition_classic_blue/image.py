import base64
from typing import ByteString
import numpy as np 
import cv2 
import re


def decode_base64(b64_image_string: str ,altcharts: bytes=b'+/') -> ByteString:
    """
    Decode base64 String to bytes 
    :param b64_image_string: base64 in string object 
    :param altcharts: altchars must be a bytes-like object or ASCII string of length 2
                      which specifies the alternative alphabet used instead of the '+' and '/'
                      characters.
    :return:  a bytes object
                        
    """

    altcharts =b'+/'
    missing_padding  = len(b64_image_string) % 4 
    if missing_padding :
        b64_image_string += '='*(4-missing_padding)
    return base64.b64decode(b64_image_string,altchars=altcharts)

 
def cv_image_to_base64(cv_frame : np.ndarray) -> ByteString:
    """
    Convert cv_image (ndarray) to bytes 
    :param cv_frame: image (ndarray)
    :return: image in bytes 
    """
   
    rgb_frame =  cv2.cvtColor(cv_frame,cv2.COLOR_RGB2BGR)
    retval,buffer = cv2.imencode('.jpg',rgb_frame)
    return base64.b64encode(buffer)

def base64_to_cv_image(base64_string : str):
    """
    Return opencv image (ndarray)
    
    :param base64_string: image base64 in string type
    :return: image in nddarray
    """
    base64_string = base64_string.split(',')[1] if len(base64_string.split(',')) > 1 else base64_string

    nparr = np.frombuffer(decode_base64(base64_string), np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    return img
