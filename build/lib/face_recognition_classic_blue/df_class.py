from typing import NoReturn,Tuple
from numpy import ndarray
import numpy as np
import pandas as pd 
from . import clustering_faces,get_cluster_center,predict_face

class  DFFaceRecognition():

    def __init__(self,dataframe : pd.DataFrame,tolerance : float=0.6) -> None:
        """
        data: a dataframe of face_encodings
        :param tolerance: How much distance between faces to consider it a match. Lower is more strict. 0.6 is typical best performance.
        """
        self.data_frame  = dataframe.copy()
        self.labels =  []
        self.clusters_centers = []
        self.clusters_centers_labels = []
        self.tolerance = tolerance


    def clustering (self ,
                    min_samples : int=5 ,
                    eps :float = 0.5,
                    metric : str="euclidean",
                    metric_params : dict = None,
                    algorithm : str="auto",
                    leaf_size : int=30,
                    p : float=None,
                    n_jobs : int = 1
                    ) -> NoReturn :         
        """
        Clustering unknown face_encodings and labeling data    
        """
        if 'label' in self.data_frame.columns : 
            # remove label column if exist -> for clustering after set_labels 
            self.data_frame = self.data_frame.drop(columns=['label'])
        
        self.data_frame = clustering_faces(self.data_frame,min_samples=min_samples,eps=eps,metric=metric,metric_params=metric_params,algorithm=algorithm,leaf_size=leaf_size,p=p,n_jobs=n_jobs)
        self.labels = self.data_frame['label'].tolist()
        self.clusters_centers,self.clusters_centers_labels = self.get_cluster_centers()
    
    def get_cluster_centers(self) -> Tuple[list,list] :
        """
        Return center_point of clustering and label for each clusters
        """
        
        data_frame = self.data_frame
        labels = self.labels
        cluster_center_labels = []
        cluster_centers = []
        for  label in set(sorted(labels)) :
            if label != -1 :
                filtered_df = data_frame[data_frame['label']==label]
                filtered_df = filtered_df.drop(columns=['label'])

                center_point = get_cluster_center((filtered_df).values.tolist())
                cluster_centers.append(center_point)
                cluster_center_labels.append(label)
        return  cluster_centers,cluster_center_labels

    def search(self,encoding : ndarray) ->  pd.DataFrame :
        """
        return a data frame for row(s) that match search encoding
        :param encoding: face_encoding that you want to search 
        
        """
        match_cluster_labels = predict_face(self.clusters_centers_labels,self.clusters_centers,encoding,tolerance=self.tolerance)
        if match_cluster_labels is not None :
            return (self.data_frame[self.data_frame['label'] == match_cluster_labels])

    def predict(self,data : pd.DataFrame) -> pd.DataFrame :
        """
        :param: data frame of face_encodings 
        return predicted labels that mapping with data (-1 mean there are not match with any groups )
        """ 
        data_list = data.values.tolist()
        results = []
        data_list = [ np.array(i) for i in data_list]
        for encoding in  data_list :
            
            match_cluster_labels = predict_face(self.clusters_centers_labels,self.clusters_centers,encoding,tolerance=self.tolerance)
            if match_cluster_labels is not None : 
                results.append(match_cluster_labels)
            else :
                results.append(-1)
        return results
    def set_labels(self,labels : list ) ->NoReturn:
        """
        :param labels: a list of ordered for each face_encoding(ndarray)s
        set labels face_data each row order
        warning : if you clustering data , label was dissapear.       
        """
        self.labels = labels
        self.data_frame['label']  = labels
        self.clusters_centers,self.clusters_centers_labels  = self.get_cluster_centers()

    def add_unknown_faces(self,data : pd.DataFrame) -> NoReturn :

        """
        add face unknown face to data and automatic mapping data with exist group 
        """
        new_labels = self.predict(data)
        new_data_frame =  data 
        new_data_frame['label'] = new_labels
        self.data_frame = pd.concat([self.data_frame,new_data_frame])
        self.clusters_centers,self.clusters_centers_labels  = self.get_cluster_centers()
    
    def add_known_faces(self,data:pd.DataFrame,labels : list) -> NoReturn :
        """
        :param  data: a list of known face_encodings that yout want to add face 
        :param labels: alist of lebel of known_face_encodings in order
        alter add face ,cluster center point are re-calculate
        """
        new_data_frame = data 
        new_data_frame['label'] = labels
        self.data_frame = pd.concat([self.data_frame,new_data_frame])
        self.labels = labels
        self.clusters_centers,self.clusters_centers_labels  = self.get_cluster_centers()