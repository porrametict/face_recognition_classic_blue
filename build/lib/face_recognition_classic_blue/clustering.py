import pandas as pd
import numpy as np 
from sklearn.cluster import DBSCAN,KMeans
from typing import Union

def grouping_clusters(data : pd.DataFrame, list_encoding : list ,cluster_label: str="label") -> dict:

    """
    Return groups of face encodings for each clusters

    :param data: DataFrame of face_encodings 
    :return: A dict of groups of clusters  by each key are cluster's id (label)  and value are list of face encoding
    """
    cluster_groups = {} 
    for face_index in range(len(data)) : 
        key  = f"{data[cluster_label][face_index]}"
        if key not in cluster_groups : 
            cluster_groups[key]  = [
                list_encoding[face_index]
            ]
        else : 
            cluster_groups[key].append(list_encoding[face_index])
    return cluster_groups  

def clustering_faces(
    face_encodings : Union[list,pd.DataFrame] ,
    cluster_label : str="label",
    return_labels : bool=False,
    min_samples : int=5 ,
    eps :float = 0.5,
    metric : str="euclidean",
    metric_params : dict = None,
    algorithm : str="auto",
    leaf_size : int=30,
    p : float=None,
    n_jobs : int = 1
    ) -> Union[list,pd.DataFrame] :
    """ 
    Return clustered face_encoding
     
    :param face_encodings:  a list of ndarray / dataframe  of face_encodings
    :param cluster_label:  column name of clusters
    :params [min_samples,eps,metric,metric_params,algorithm,leaf_size,p,n_jobs]: DBSCAN params
    :return: clustered face_encoding in data_frame. if return_labels is True return a list of labels only
    """

    if isinstance(face_encodings,list):
        df = pd.DataFrame(face_encodings)
    else :
        df = face_encodings
    
    clustering = DBSCAN(min_samples=min_samples,eps=eps,metric=metric,metric_params=metric_params,algorithm=algorithm,leaf_size=leaf_size,p=p,n_jobs=n_jobs).fit(df)
    df[cluster_label] = clustering.labels_
    if return_labels :
        return df[cluster_label].tolist()
    return df 

def get_cluster_center(face_encodings : list )  -> list : 
    """
    Return center point of face_encoding cluster
    
    :param face_encoding: a list of ndarray of same cluster
    :return: a list of center_points of face_encoding cluster in 128 dimentions
    """

    return np.mean(face_encodings,axis=0)
 