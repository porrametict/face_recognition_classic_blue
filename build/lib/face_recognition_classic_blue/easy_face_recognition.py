from typing import Any
import numpy as np 
import math 
import face_recognition

def predict_face(known_faces:list,known_face_encodings:list, face_encoding : np.ndarray ,tolerance : float=0.6) -> Any:  
    """
    Return  know face item that match with face that given
    
    :param known_faces: list of any data that can identify each face
    :param know_face_encodings: list of face_encoding of known faces order 
    :param face_encoding: ndarray of face that you want to find 
    :param tolerance: How much distance between faces to consider it a match. Lower is more strict. 0.6 is typical best performance.
    :return know_face item in known_faces that match to face_encoding:
    """
    
    matchs = face_recognition.compare_faces(known_face_encodings,face_encoding,tolerance=tolerance)
    face_distances = face_recognition.face_distance(known_face_encodings,face_encoding)
    if len(face_distances) :
        best_match_index = np.argmin(face_distances)

        if matchs[best_match_index] : 
            return known_faces[best_match_index]
    return None


def filter_face_locations(face_locations:list,min_width_threshold : int=100,min_height_threshold : int=100) -> list :
    """
    Return  a list of face_locations that passed thresholds

    :param face_locations: list of tuple of face_location 
    :param min_width_theshold: number of minimun face_location width 
    :param min_height_threshold: num of minimun face_location height
    """

    filtered_face_locations = []

    for face_location in face_locations :
        (top, right, bottom, left) = face_location

        if  (abs(top - bottom) < min_height_threshold ) or (abs(left - right ) < min_width_threshold ) : 
            continue 
        else : 
            filtered_face_locations.append(face_location)
    return filtered_face_locations


def min_distance (location : tuple ,locations : list ) -> float:
    """
    Return nearest location's distance  of location that you want to find it  
    
    :param location: a tople of location that you want to find a nearest location 
    :param locations: a list of tuple of your locations 
    :return: a float of distances between your're location and nearest location in locations list 
    """

    min_dist  = math.dist(location,locations[0])
    for tmp_location in locations :
        dist = math.dist(location,tmp_location)
        if dist < min_dist :  
            min_dist = dist
    return min_dist         
    

def find_diffrence_locations_distances(prev_locations : list ,locations : list )-> list  : 
    """
    Return a list of  distance nearest previous_location for each location
    :param prev_location: a list of tuple of previous locations
    :param  location: a list of current locations
    """
    if len(prev_locations) > 0 : 
        return  [ min_distance(curr_location,prev_locations) for curr_location in locations]
    else :
        return  list()

def predict_faces(face_data :list , face_encodings: list ,unknown_face_encodings : list ,tolerance : float=0.6) -> list :
    """
    Predict unknown faces from known faces 
    :param face_data: a list of data that belong face_encodings ordered
    :param face_encoding: a list of ndarray of knonw face_encoding
    :param unknown_face_encoding: a list of ndarray of unknown face_encoding
    :param tolerance: How much distance between faces to consider it a match. Lower is more strict. 0.6 is typical best performance.
    """
    return [predict_face(face_data,face_encodings,unknown_face,tolerance=tolerance) for unknown_face in unknown_face_encodings  ]