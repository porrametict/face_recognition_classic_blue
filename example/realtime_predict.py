import cv2 
from face_recognition_classic_blue.easy_face_recognition import predict_faces
import face_recognition
from get_ex_file import get_ex_data

# get known faces"
# known_faces wil be a list of any objects that can identify known face_encoding ,it can be name or some id
# known_faces_encodings  will be a list of face_encoding(ndarray) | you can get it by use face_recognition library
known_faces,known_faces_encodings = get_ex_data()

# get media capture 
media_capture  =cv2.VideoCapture(0)

while True :
    ret,frame = media_capture.read()
    rgb_frame = frame[:, :, ::-1] # convert bgr to rgb
    face_locations = face_recognition.face_locations(rgb_frame) 
    face_encodings = face_recognition.face_encodings(frame,face_locations)

    # predict face return  a list of know-face that match with face_encodings 
    # for some face_encoding is not match with any know_face_encodings it will be None 
    faces = predict_faces(known_faces,known_faces_encodings,face_encodings)
    faces = [face if face is not  None else "Unknown" for face in faces]

    # Display the results
    for (top, right, bottom, left), name in zip(face_locations, faces):

        # Draw a box around the face
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # Draw a label with a name below the face
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    # Display the resulting image
    cv2.imshow('Video', frame)

    #  press q for exit
    if cv2.waitKey(1) & 0xFF == ord("q") : 
        break


media_capture.release()