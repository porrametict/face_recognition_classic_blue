import os
import face_recognition

def get_ex_data ():
    known_faces = []
    known_faces_encodings = []
    directory = r'example/ex_data' # path to youfr face images
    for filename in os.listdir(directory) :
        if filename.endswith(".jpg") or filename.endswith(".png") :
            path = (os.path.join(directory, filename))
            
            load_image = face_recognition.load_image_file(path)
            face_encodings = face_recognition.face_encodings(load_image)
            
            tmp_encoding = face_encodings
            if len(tmp_encoding):
                known_faces.append(filename)
                known_faces_encodings.append(tmp_encoding[0])
    return known_faces,known_faces_encodings