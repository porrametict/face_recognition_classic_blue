from flask import Flask,request
from face_recognition_classic_blue import ListFaceRecognition,predict_face
import face_recognition_classic_blue as face_cb
import  face_recognition

from get_ex_file import  get_ex_data

app = Flask(__name__)

# get known_find faces
known_faces,known_faces_encodings = get_ex_data()

face_obj = ListFaceRecognition(known_faces_encodings,0.4)

# make dummy profiles
d_label = ["Note","Toon"]
d_labels=[]
d_profile = []
for i in known_faces :
    name = None 
    if 'note' in i :
        name = "Note"
        d_labels.append(0)
    elif 'toon' in i :
        name = "Toon"
        d_labels.append(1)
    else :
        d_labels.append(-1)
    d_profile.append({
        "name" : name,
        "source" : i
    })


face_obj.set_labels(d_labels)


@app.route('/')
def index():
     return "hi"

@app.route('/search',methods=['POST'])
def search() :
    """
    Given a data of face_encodings that match to image
    """

    # Grab an image
    request_image = request.get_json()
    res_image = request_image['image']
    
    #Convert base64image to ndrray
    image = face_cb.base64_to_cv_image(res_image)
    # find face location
    face_locations = face_recognition.face_locations(image)
    #  encoding face and grab one face encoding
    face_encoding = face_recognition.face_encodings(image,face_locations)[0]
    # predict face  : got labels
    label_results = face_obj.predict([face_encoding])
    
    # get profile form label
    match_items  = [d_profile[i] for i in  range(len(d_labels)) if d_labels[i] == label_results[0]]
    return {"data" : match_items}

@app.route('/predict',methods=['POST'])
def predict() :    
    """
    Given a name of face_encodings that match to image
    """

    request_image = request.get_json()
    res_image = request_image['image']
    image = face_cb.base64_to_cv_image(res_image)
    face_locations = face_recognition.face_locations(image)
    face_encoding = face_recognition.face_encodings(image,face_locations)[0]

    
    # predict face and get match face's name
    label_results = face_obj.predict([face_encoding])
    result = d_label[label_results[0]] if  label_results[0] in d_label else "Unknown"

    return  {"predict" : result}

@app.route('/add_face',methods=['POST'])
def add_face():
    """
    Add known face to class instance
    """

    request_data = request.get_json()
    res_image = request_data['image']
    res_label = request_data['label'] 
    image = face_cb.base64_to_cv_image(res_image)
    face_locations = face_recognition.face_locations(image)
    face_encoding = face_recognition.face_encodings(image,face_locations)[0]
    
    # add_known_face require list of face_encodings and list of face label
    face_obj.add_known_faces(data=[face_encoding],labels=[res_label])
    
    return "success"

if __name__ == "__main__" :
    app.run(host="0.0.0.0",port="5000",debug=True)