from face_recognition_classic_blue import base64_to_cv_image,decode_base64,cv_image_to_base64



# Get some base64 strings
f = open("example/ex_data/base64.txt")
b64_text = f.read()
f.close()

# Convert base64 to ndarray (suitable with opencv lib)
cvimg = base64_to_cv_image(b64_text)

# Convert cv_image (ndarray)  to base64
b_image = cv_image_to_base64(cvimg)

#  Convert base64 string To Image (return a bytes object for write file)
image = decode_base64(b64_text)