from face_recognition_classic_blue import DFFaceRecognition
import pandas as pd 

from get_ex_file import get_ex_data

# get known faces"
# known_faces wil be a list of any objects that can identify known face_encoding ,it can be name or some id
# known_faces_encodings  will be a list of face_encoding(ndarray) | you can get it by use face_recognition library
known_faces,known_faces_encodings = get_ex_data()



unknow_face = known_faces.pop(1)
unknow_face_encoding = known_faces_encodings.pop(1)

# create data frame from face_encondings list
df = pd.DataFrame(known_faces_encodings)

# create class instance | argument is a dataframe
obj = DFFaceRecognition(df)

# your data frame will store in data_frame attribute
print(obj.data_frame)
# if you have a list of labels , you can set by using set_labels methods . plase make sure len(your labels) == len(dataframe'row)
obj.set_labels(known_faces)
# if you  don't known labels  ,you can use clustering method for clustering data 
# obj.clustering() # uncomment this line to use it

# after clustering data, instance will has 3 attribute
# 1 .labels attribute is a list of cluster label , if label is -1 mean that encoding not have a group
print(obj.labels) 
# 2. cluster_centers attribute is a list of center point of group 
print(obj.clusters_centers)
# 3.cluster_center_label  attribute is a  list of all cluster center label (-1 not included)
print(obj.clusters_centers_labels)



# the label cluster will show in data frame in label column
print(obj.data_frame)


#### before use below methods, your data must have labels ###

# for search similar 
# return dataframe of sililar encodings
print(obj.search(unknow_face_encoding))

# for predict the groups of thoses encodings 
df2 =  df.copy()
df2_predict = obj.predict(df2)
print(df2_predict)


# you can add unknown face after initial class instance 
# face will grouping with exist groups 

obj.add_unknown_faces(df2)

# if you known a label use can assign a label like this
# plase make sure your labels are match in exist labels , Otherwise it will be a new label

obj.add_known_faces(df2,df2_predict)

# Warning if your want to clustering again , the labels are reset 
# your can set new labels by set labels again 
 
obj.clustering()

new_label =  ["unknown" if label == -1 else f'group {label}' for label in obj.labels]
 
obj.set_labels(new_label)

print(obj.data_frame)