from face_recognition_classic_blue import clustering_faces,grouping_clusters,get_cluster_center
from get_ex_file import  get_ex_data

# get known faces"
# known_faces wil be a list of any objects that can identify known face_encoding ,it can be name or some id
# known_faces_encodings  will be a list of face_encoding(ndarray) | you can get it by use face_recognition library
known_faces,known_faces_encodings = get_ex_data()


# clustering  face_encoding, you will get a dataframe that has 128 dimansion of face_encoding and label dimansion
# each row  in label column are integer start  with  0 to n  (-1 means that row dosen't have a cluster)
clusters = clustering_faces(known_faces_encodings)

print(clusters)

# you can convert clusters to dict 
# key is a cluster labels in string object . ex : '0'
# value is a list of encoding that have label matches that key
# Warning '-1' are included
groups = grouping_clusters(clusters,known_faces_encodings)

print(groups) 

# if you want to get only labels,put return_labels=True into function 
# you will get a list of labels
clusters =  clustering_faces(known_faces_encodings,return_labels=True)

print(clusters) 

# this function will return a ndarray of center_point (mean value) of cluster
center_point = get_cluster_center(groups['0'])
print(center_point)

