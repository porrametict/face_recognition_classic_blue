from face_recognition_classic_blue import predict_face,predict_faces
from get_ex_file import get_ex_data

# get known faces"
# known_faces wil be a list of any objects that can identify known face_encoding ,it can be name or some id
# known_faces_encodings  will be a list of face_encoding(ndarray) | you can get it by use face_recognition library
known_faces,known_faces_encodings = get_ex_data()

# predict face
unknow_face = known_faces.pop(1)
unknow_face_encoding = known_faces_encodings.pop(1)
# use predict_face for predict only one face, the result of prediction is an object known_faces
predict = predict_face(known_faces,known_faces_encodings,unknow_face_encoding)
print(predict)

# use predict_faces for predict many faces
results =  predict_faces(known_faces,known_faces_encodings,known_faces_encodings)
print(results)
print(results == known_faces)

