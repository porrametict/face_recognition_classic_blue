from setuptools import find_packages, setup
setup(
    name='face_recognition_classic_blue',
    packages=find_packages(include=['face_recognition_classic_blue']),
    version='0.0.1',
    description='My first Python library',
    author='Me',
    license='MIT',
    install_requires=[
        'face-recognition==1.3.0',
        'opencv-contrib-python==4.5.1.48',
        'pandas==1.2.2',
        'scikit-learn==0.24.1'
    ],
    setup_requires=['pytest-runner'],
    )