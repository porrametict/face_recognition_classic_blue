import cv2 
import os 

def get_media(rtsp_uri : str , protocol : str ) -> cv2.VideoCapture : 
    os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = f"rtsp_transport;{protocol}"
    return cv2.VideoCapture(rtsp_uri, cv2.CAP_FFMPEG)

def get_rtsp_media (rtsp_uri : str ) -> cv2.VideoCapture : 
    media_capture = get_media(rtsp_uri , "udp")
    ret_first , first_frame  =media_capture.read()
    if not ret_first : 
        media_capture.release()
        media_capture = get_media(rtsp_uri,"tcp")

    return media_capture        