## Face Recognition Classic Blue

The module provides an easy manipulation for [face recognition](https://github.com/ageitgey/face_recognition)

### Installation

Requirements 
 - macOS or Linux (Some features do not support on Windows)
 - Python 3.3 or later
 - face_recognition library (please make sure you follow on[face_recognition installation ](https://github.com/ageitgey/face_recognition/blob/master/README.md) ) 

then install use pip3 :
```
pip3 install git+https://gitlab.com/porrametict/face_recognition_classic_blue.git
```

this module doesn't upload to PyPI for any reason.

### Class usage

this module contains 2 main classes for use with different data sources.
 - DFFaceRecognition
 - ListFaceRecognition

theses class have same features and methods but arguments and return result are not same, for DFFaceRecognition are work with Pandas DataFrame and ListFaceRecognition are work with List.

##### initialize class instance
for initial instance, you have to pass one argument, that a face_encoding data. for this instruction, I will show you for use ListFaceRecognition.
```
import face_recgonition
from face_recognition_classic_blue import ListFaceRecognition

known_face_encodings = []
known_faces = []

# load and encoding face


john_image  = face_recognition.load_image_file("/path/to/john_image.jpg")
john_face_encoding = face_recognition.face_encodings(john_image)[0]


joe_image  = face_recognition.load_image_file("/path/to/joe_image.jpg")
joe_face_encoding = face_recognition.face_encodings(joe_image)[0]

known_face_encodings.append(john_encoding)
known_faces.append("John")

known_face_encodings.append(joe_encoding)
known_faces.append("Joe")


# initial here 
obj =  ListFaceReconition(known_face_encodings)

```
After initial instance, you have 2 alternative ways to take with it.
1. if you known labels for all data. you can set them by use set_lavels method. 
```
obj.set_labels(known_faces)  #  in this case , known_faces are label
```
2. if you don't know labels, you can use clustering methods these methods will make clusters and labeling them. before use, clustering makes sure you have enough data.
P.S. clustering use [scikit-learn DBSCAN](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.DBSCAN.html) you can pass DBSCAN arguments.

```
obj.clustering()
```
then you can use these attributes. 
```
print(obj.labels)
print(obj.custers_centers)
print(obj.clusters_center_labels)
```
##### seaching 
Search methods will compare encoding with custer_centers then return a list of encodings within that cluster.
```
search_encoding  = known_face_encodings[0]
match_encodings  = obj.search(search_encoding)
print(match_encodings)
````

##### prediction
Prediction methods will return a list of labels of existing clusters, if you got -1 that means face_encodings that position does not match with any existing clusters.
```
predict_face = known_face_encodings[0]
predict_faces = [predict_face] # arguments are a list of face encodings

predict_results = obj.predict(predict_faces) 
```

##### add faces
you can add face_encodings after the initial instance to improve clusters, add faces methods always re-calculate the cluster_centers. you can add face in 2 alternative ways.
1. add unknown faces


```
unknown_face_encodings = known_face_encodings # assume these are unknown face
obj.add_unknown_faces(unknown_face_encodings)  # unknown face will automatically map with existing groups
````

2. add known faces
```
known_face_encodings_2 = known_face_encodings # assume these are another face
known_face_2  = knonw_face # assume these are another face

obj.add_known_faces(known_face_encodings_2,known_face_2)

```
### Function usage

##### predict face
you can predict face in an easy way by use ```predict_face``` function. this method will return data[position] where the position is known_face_encoding's position that matches with unknown_face_encoding. if you want to predict many faces, you can pass a list of unknown face encodings into ```predict_faces```
```
from face_recognition_classic_blue import predict_face

known_face_data_list = {{list of face names}}
known_face_encodings = {{list of face encodings}}
unknown_face_encoding  = {{ndarray of a face_encoding}}

predict_result = predict_face(known_face_data_list,known_face_encodings,unknown_face_encding)
```

##### clustering face
you can clustering face by use ```clustering_face``` function. this function will clustering faces by use [scikit-learn DBSCAN](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.DBSCAN.html) you can pass DBSCAN arguments. the function will return a dataframe, if you want to get only labels for each face you can pass ```return_labels=True``` into the function. 
```
from face_recognition_classic_blue import clustering_faces,grouping_clusters

unknown_face_encodings =  {{list of face encodings}}

# clustering_faces will return  dataframe
clusters = clustering_faces(unknonwn_face_encodings)

# convert each clusters to dict
groups =  grouping_clusters(clusters,unknown_face_encodings)

# if you want to get only labels 
clusters = clustering_faces(unknonwn_face_encodings,return_labels=True)

```
 
### Example

###### class usages

* [DFFacerecognition](https://gitlab.com/porrametict/face_recognition_classic_blue/-/blob/master/example/use_dfclass_example.py)

* [ListFacerecognition](https://gitlab.com/porrametict/face_recognition_classic_blue/-/blob/master/example/use_listclass_example.py)

* [API web services (Flask)](https://gitlab.com/porrametict/face_recognition_classic_blue/-/blob/master/example/flask_api.py)

###### function usages

* [Face clustering](https://gitlab.com/porrametict/face_recognition_classic_blue/-/blob/master/example/clustering.py)
 
* [Face prediction](https://gitlab.com/porrametict/face_recognition_classic_blue/-/blob/master/example/predict_faces.py)

* [Realtime Prediction](https://gitlab.com/porrametict/face_recognition_classic_blue/-/blob/master/example/realtime_predict.py)

* [base64 convertor](https://gitlab.com/porrametict/face_recognition_classic_blue/-/blob/master/example/convert_base64.py)

